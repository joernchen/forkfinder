PLUGINS := $(wildcard ./plugins/*/*.go)

.PHONY all: tool plugins

tool:
	go build -o build/ethan ethan.go

$(PLUGINS):
	go build -buildmode=plugin -o ./build/$(@:.go=.so)  $@

.PHONY plugins: $(PLUGINS)

clean:
	rm -rf build/*
