package main

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"sync"

	"github.com/shurcooL/graphql"
	gitlab "gitlab.com/gitlab-org/api/client-go"
)

var Instance string
var GraphqlClient graphql.Client
var GitlabClient gitlab.Client
var PluginCommand = "keyleecher"
var PluginDesc = "This command fetches usernames and their SSH keys from a GitLab instance"
var PluginHelp = fmt.Sprintf("%s %s", os.Args[0], PluginCommand)

const (
	numWorkers = 15 // Number of concurrent workers
)

type User struct {
	Username string
	ID       string
}

func fatal(err error) {
	fmt.Fprintf(os.Stderr, "%s: %s\n", os.Args[0], err)
	os.Exit(1)
}
func fetchUserKeys(user User) ([]string, error) {

	keys, _, err := GitlabClient.Users.ListSSHKeysForUser(string(user.Username), &gitlab.ListSSHKeysForUserOptions{})

	if err != nil {
		return nil, err
	}

	var keyStrings []string
	for _, key := range keys {
		keyStrings = append(keyStrings, key.Key)
	}

	return keyStrings, nil
}
func worker(id int, jobs <-chan User, wg *sync.WaitGroup, keysDir string) {
	defer wg.Done()
	for user := range jobs {
		fmt.Printf("Worker %d: Fetching keys for user: %s\n", id, user.Username)

		keys, err := fetchUserKeys(user)
		if err != nil {
			fmt.Printf("Worker %d: Error fetching keys for %s: %v\n", id, user.Username, err)
			continue
		}

		if len(keys) > 0 {
			filename := filepath.Join(keysDir, user.Username+".keys")
			content := ""
			for _, key := range keys {
				content += fmt.Sprintf("%s\n", key)
			}
			err = os.WriteFile(filename, []byte(content), 0644)
			if err != nil {
				fmt.Printf("Worker %d: Error writing keys for %s: %v\n", id, user.Username, err)
				continue
			}

			fmt.Printf("Worker %d: Keys for %s saved to %s\n", id, user.Username, filename)
		} else {
			fmt.Printf("Worker %d: No keys found for %s\n", id, user.Username)
		}
	}
}

func RunCheck() {
	var endCursor *graphql.String
	hasNextPage := true

	keysDir := "user_keys"
	err := os.MkdirAll(keysDir, 0755)
	if err != nil {
		fatal(err)
	}

	jobs := make(chan User, 100)
	var wg sync.WaitGroup

	// Start worker goroutines
	for w := 1; w <= numWorkers; w++ {
		wg.Add(1)
		go worker(w, jobs, &wg, keysDir)
	}

	for hasNextPage {
		var query struct {
			Users struct {
				Nodes []struct {
					Username graphql.String
					ID       graphql.ID
				}
				PageInfo struct {
					EndCursor   graphql.String
					HasNextPage graphql.Boolean
				}
			} `graphql:"users(humans: true, first: 50, sort: CREATED_DESC, after: $endCursor)"`
		}

		variables := map[string]interface{}{
			"endCursor": endCursor,
		}

		err := GraphqlClient.Query(context.Background(), &query, variables)
		if err != nil {
			fatal(err)
		}

		for _, user := range query.Users.Nodes {
			jobs <- User{
				Username: string(user.Username),
			}
		}

		hasNextPage = bool(query.Users.PageInfo.HasNextPage)
		if hasNextPage {
			endCursor = &query.Users.PageInfo.EndCursor
		}
	}

	close(jobs)
	wg.Wait()

	fmt.Println("Key fetching completed for all users.")
}
