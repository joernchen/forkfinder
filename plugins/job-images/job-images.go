package main

import (
	"bytes"
	"fmt"
	"os"
	"regexp"
	"strconv"

	"github.com/shurcooL/graphql"
	gitlab  "gitlab.com/gitlab-org/api/client-go"

)

var Instance string              // target instance will be set before invoking RunCheck
var GitlabClient gitlab.Client   // GitLab API client will be set before invoking RunCheck
var GraphqlClient graphql.Client // GitLab Graphql API client will be set before invoking RunCheck

var PluginCommand = "job-images"                                                                                                                     // command to be called via the CLI
var PluginDesc = "This command will list the used Docker images in the last depth*100 CI jobs within a project `job-images path/to/project [depth]`" // description of the command
var REGEXP = regexp.MustCompile(`Using docker image (\S*) for (?<requested>\S*) with digest (?<fullimage>\S*)`)
var PluginHelp = fmt.Sprintf("%s %s", os.Args[0], PluginCommand) // usage / help text

func fatal(err error) {
	fmt.Fprintf(os.Stderr, "%s: %s\n", os.Args[0], err)
	os.Exit(1)
}

func RunCheck() {
	target := os.Args[2] // target is a group ID or name
	depth, err := strconv.Atoi(os.Args[3])
	if err != nil {
		fatal(err)
	}
	opts := gitlab.ListJobsOptions{ListOptions: gitlab.ListOptions{Page: 1, PerPage: 100}}
	project, _, err := GitlabClient.Projects.GetProject(target, nil)
	if err != nil {
		fatal(err)
	}

	fmt.Fprintf(os.Stderr, "[+] Scanning project: %s\n", project.PathWithNamespace)
	for i := 1; i <= depth; i = opts.Page {
		jobs, response, err := GitlabClient.Jobs.ListProjectJobs(project.ID, &opts)
		if err != nil && !(response.StatusCode == 404 || response.StatusCode == 403) {
			fatal(err)
		}
		opts.Page += 1
		for _, job := range jobs {
			trace, _, err := GitlabClient.Jobs.GetTraceFile(project.ID, job.ID)
			if err != nil {
				continue
			}
			buff := bytes.NewBuffer([]byte{})
			trace.WriteTo(buff)
			//			fmt.Print(buff.String())
			images := REGEXP.FindAllStringSubmatch(buff.String(), -1)
			fullimage := REGEXP.SubexpIndex("fullimage")
			requested := REGEXP.SubexpIndex("requested")
			if images == nil {
				continue
			}
			for _, image := range images {
				fmt.Println(image[requested] + " " + image[fullimage])
			}
		}

	}
}
