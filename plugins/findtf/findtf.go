package main

import (
	"archive/zip"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"main/plugins/findtf/planfile"
	"os"
	"strconv"
	"time"

	"github.com/shurcooL/graphql"
	msgpack "github.com/vmihailenco/msgpack/v5"
	"gitlab.com/gitlab-org/api/client-go"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v4"
	"google.golang.org/protobuf/proto"
)

var Instance string              // target instance will be set before invoking RunCheck
var GitlabClient gitlab.Client   // GitLab API client will be set before invoking RunCheck
var GraphqlClient graphql.Client // GitLab Graphql API client will be set before invoking RunCheck

var PluginCommand = "findtf"                                                                                                                                   // command to be called via the CLI
var PluginDesc = "This command will find exposed secrets in terraform states leaked in job artifacts use `findtf user ausername` or `findtf group agroupname`" // description of the command

var PluginHelp = fmt.Sprintf("%s %s", os.Args[0], PluginCommand) // usage / help text

func fatal(err error) {
	fmt.Fprintf(os.Stderr, "%s: %s\n", os.Args[0], err)
	os.Exit(1)
}

var vulns []report.Vulnerability

type Var struct {
	Type  string
	Value interface{} `msgpack:",omitempty"`
}

func checkPlanCache(cacheFile io.Reader, project *gitlab.Project, job *gitlab.Job) {

	buff := bytes.NewBuffer([]byte{})
	size, err := io.Copy(buff, cacheFile)
	if err != nil {
		fatal(err)
	}

	reader := bytes.NewReader(buff.Bytes())
	zipReader, err := zip.NewReader(reader, size)
	if err != nil {
		fatal(err)
	}
	for _, f := range zipReader.File {
		if f.Name == "tfplan" {

			plan, err := f.Open()
			if err != nil {
				fatal(err)
			}
			in, err := ioutil.ReadAll(plan)
			if err != nil {
				fatal(err)
			}
			pf := &planfile.Plan{}
			err = proto.Unmarshal(in, pf)
			if err != nil {
				fatal(err)
			}
			if len(pf.Variables) > 0 {
				fmt.Printf("# %s\n", project.PathWithNamespace)
				fmt.Printf("## [Job %d](%s)\n", job.ID, job.WebURL)
				fmt.Println("| Variable | Value | Check |")
				fmt.Println("|---|---|---|")

				for k, v := range pf.Variables {
					var value Var

					err := msgpack.Unmarshal(v.Msgpack, &value)
					if err != nil {
						fatal(err)
					}
					if value.Type == "\"string\"" {

						fmt.Printf("| %s | %s | ? |\n", k, value.Value)

					}
				}
				fmt.Println()
			}
		}
	}

}

func contains(s []int, e int) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func allProjects(projects []*gitlab.Project, depth int) {
	for _, project := range projects {
		if contains(doneprojects, project.ID) {
			continue
		}

		opts := gitlab.ListJobsOptions{ListOptions: gitlab.ListOptions{Page: 1, PerPage: 100}}

		fmt.Fprintf(os.Stderr, "[+] Scanning project: %s\n", project.PathWithNamespace)
		for i := 1; i <= depth; i = opts.Page {
			jobs, response, err := GitlabClient.Jobs.ListProjectJobs(project.ID, &opts)
			if err != nil && !(response.StatusCode == 404 || response.StatusCode == 403) {
				fatal(err)
			}
			for i := range jobs {
				for j := range jobs[i].Artifacts {

					if jobs[i].Artifacts[j].Filename == "artifacts.zip" || jobs[i].Artifacts[j].Filename == "plan.zip" {
						reader, response, err := GitlabClient.Jobs.DownloadSingleArtifactsFile(project.ID, jobs[i].ID, "plan.cache")

						if err != nil && !(response.StatusCode == 404 || response.StatusCode == 403) {
							fatal(err)
						}
						if response.StatusCode == 200 {
							checkPlanCache(reader, project, jobs[i])
						}
						reader, response, err = GitlabClient.Jobs.DownloadSingleArtifactsFile(project.ID, jobs[i].ID, "tfplan.tfplan")

						if err != nil && !(response.StatusCode == 404 || response.StatusCode == 403) {
							fatal(err)
						}
						if response.StatusCode == 200 {
							checkPlanCache(reader, project, jobs[i])
						}
						reader, response, err = GitlabClient.Jobs.DownloadSingleArtifactsFile(project.ID, jobs[i].ID, "projects/plan.cache")

						if err != nil && !(response.StatusCode == 404 || response.StatusCode == 403) {
							fatal(err)
						}
						if response.StatusCode == 200 {
							checkPlanCache(reader, project, jobs[i])
						}
					}
				}
			}
			if response.NextPage == 0 {
				break
			}
			opts.Page = response.NextPage
		}
		doneprojects = append(doneprojects, project.ID)
	}
}
func True() *bool {
	b := true
	return &b
}

var doneprojects = make([]int, 0)

func RunCheck() {
	target := os.Args[3] // target is a group ID or name
	ttype := os.Args[2]  // type is user or group
	depth := 1
	var err error
	if len(os.Args) > 4 { // depth is how many pages of 100 jobs should we go
		depth, err = strconv.Atoi(os.Args[4])
		if err != nil {
			fatal(err)
		}
	}
	switch t := ttype; t {
	case "group":
		opt := &gitlab.ListGroupProjectsOptions{
			IncludeSubGroups: True(),
			ListOptions: gitlab.ListOptions{
				PerPage: 10,
				Page:    1,
			},
		}
		for {
			projects, resp, err := GitlabClient.Groups.ListGroupProjects(target, opt)
			if err != nil {
				fatal(err)
			}
			allProjects(projects, depth)
			if resp.NextPage == 0 {
				break
			}
			opt.Page = resp.NextPage
		}

	case "user":
		opt := &gitlab.ListProjectsOptions{
			ListOptions: gitlab.ListOptions{
				PerPage: 10,
				Page:    1,
			},
		}
		for {
			projects, resp, err := GitlabClient.Projects.ListUserProjects(target, opt)

			if err != nil {
				fatal(err)
			}
			allProjects(projects, depth)
			if resp.NextPage == 0 {
				break
			}
			opt.Page = resp.NextPage
		}
	}
	out := report.NewReport()
	out.Vulnerabilities = vulns
	timenow := report.ScanTime(time.Now())
	out.Remediations = make([]report.Remediation, 0)
	vendor := report.Vendor{Name: "E.T.H.A.N. Terraform plan cache Scanner"}
	details := report.ScannerDetails{ID: "ethan_terraform_plan_cache", Name: "E.T.H.A.N. Terraform plan cache Scanner", Version: "0.0.1", Vendor: vendor}
	analyzer := report.AnalyzerDetails{ID: "ethan_terraform_plan_cache", Vendor: vendor, Version: "0.0.1", Name: "E.T.H.A.N. Terraform plan cache Scanner"}
	out.Scan = report.Scan{
		Analyzer:  analyzer,
		Scanner:   details,
		Type:      "secret_detection",
		StartTime: &timenow,
		EndTime:   &timenow,
		Status:    "success",
	}
	output, _ := json.Marshal(out)
	fmt.Println(string(output))
}
