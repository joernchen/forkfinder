package main

import (
	"context"
	"fmt"
	"github.com/shurcooL/graphql"
	"gitlab.com/gitlab-org/api/client-go"

	"os"
	"strings"
)

var Instance string
var GitlabClient gitlab.Client
var GraphqlClient graphql.Client

var PluginCommand = "forkfinder"
var PluginDesc = "This command let's you find forks of non-public projects in a group and its subgroups"

var PluginHelp = fmt.Sprintf("%s %s groupname", os.Args[0], PluginCommand)

func fatal(err error) {
	fmt.Fprintf(os.Stderr, "%s: %s\n", os.Args[0], err)
	os.Exit(1)
}

func RunCheck() {

	target := os.Args[2]
	endCursor := ""
	var privateWithFork []graphql.ID

	for more := true; more; {
		var query struct {
			Group struct {
				Projects struct {
					Edges []struct {
						Node struct {
							Id         graphql.ID
							FullPath   graphql.String
							ForksCount graphql.Int
							Visibility graphql.String
						}
					}
					PageInfo struct {
						EndCursor   graphql.String
						HasNextPage graphql.Boolean
					}
				} `graphql:"projects(includeSubgroups: true, after: $endCursor)"`
			} `graphql:"group(fullPath: $mygroup)"`
		}
		vars := map[string]interface{}{
			"endCursor": graphql.String(endCursor),
			"mygroup":   graphql.ID(target),
		}

		err := GraphqlClient.Query(context.Background(), &query, vars)
		if err != nil {
			fatal(err)
		}
		for i := range query.Group.Projects.Edges {
			if query.Group.Projects.Edges[i].Node.ForksCount > 0 && query.Group.Projects.Edges[i].Node.Visibility != "public" {
				idstr := query.Group.Projects.Edges[i].Node.Id.(string)
				idarr := strings.Split(idstr, "/")
				id := idarr[len(idarr)-1]
				privateWithFork = append(privateWithFork, id)
			}

		}
		more = bool(query.Group.Projects.PageInfo.HasNextPage)
		endCursor = string(query.Group.Projects.PageInfo.EndCursor)
	}
	for i := range privateWithFork {
		project, _, err := GitlabClient.Projects.GetProject(privateWithFork[i], nil)
		if err != nil {
			fatal(err)
		}
		forks, _, err := GitlabClient.Projects.ListProjectForks(privateWithFork[i], nil)
		if err != nil {
			fatal(err)
		}
		fmt.Printf("Project %s/%s is %s and has %d forks:\n", Instance, project.PathWithNamespace, project.Visibility, project.ForksCount)
		for i := range forks {
			fmt.Printf("\t%s/%s\n", Instance, forks[i].PathWithNamespace)

		}
	}

}
