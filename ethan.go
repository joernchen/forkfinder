package main

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"plugin"

	"github.com/shurcooL/graphql"
	"gitlab.com/gitlab-org/api/client-go"
	"golang.org/x/oauth2"
)

func fatal(err error) {
	fmt.Fprintf(os.Stderr, "%s: %s\n", os.Args[0], err)
}

func allPlugins() (plugins []string) {
	ex, err := os.Executable()
	if err != nil {
		fatal(err)
	}
	exPath := filepath.Dir(ex)
	plugPath := filepath.Join(exPath, "plugins")
	err = filepath.Walk(plugPath, func(path string, info os.FileInfo, err error) error {
		if filepath.Ext(path) == ".so" {
			plugins = append(plugins, path)
		}
		return nil
	})
	if err != nil {
		fatal(err)
	}
	return plugins
}
func findPlugin(command string) (plug *plugin.Plugin) {

	plugins := allPlugins()
	for _, check := range plugins {

		plug, err := plugin.Open(check)
		if err != nil {
			fatal(err)
		}

		plugCmd, err := plug.Lookup("PluginCommand")
		if err != nil {
			fmt.Println(fmt.Printf("Error loading plugin: %s", check))
			fmt.Println(err)
			continue
		}

		if command == *plugCmd.(*string) {
			return plug
		}
	}
	fmt.Println("Plugin not found: ", command)
	os.Exit(1)
	return nil
}

func printHelp() {
	if len(os.Args) > 2 {
		plug := findPlugin(os.Args[2])
		name, _ := plug.Lookup("PluginHelp")
		desc, _ := plug.Lookup("PluginDesc")
		fmt.Print(*name.(*string))
		fmt.Print(":\t\t")
		fmt.Println(*desc.(*string))
	} else {
		fmt.Printf("E.T.H.A.N. - *E*nhanced *T*ool for *H*ardening and *A*uditing an I*N*stance: \n\nUsage:\n\n%s help [pluginname] - display the help (for pluginname if given)\n%s list - list all plugins\n", os.Args[0], os.Args[0])
	}
}

func listPlugins() {

	fmt.Println("All available commands: \n\nhelp\nlist\n\nPlugins:\n")
	plugins := allPlugins()
	for _, check := range plugins {
		plug, err := plugin.Open(check)
		if err != nil {
			fatal(err)
		}
		name, _ := plug.Lookup("PluginCommand")
		fmt.Println(*name.(*string))
	}
}

func loadAndRun(command string) {
	var instance = os.Getenv("GL_API_HOST")
	var accessToken = os.Getenv("GL_API_TOKEN")
	var graphqlEndpoint = fmt.Sprintf("%s/api/graphql", instance)
	var apiEndpoint = fmt.Sprintf("%s/api/v4/", instance)

	// graphql API client setup
	src := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: accessToken},
	)
	httpClient := oauth2.NewClient(context.Background(), src)
	graphqlClient := graphql.NewClient(graphqlEndpoint, httpClient)

	// REST API client setup
	gitlabClient, err := gitlab.NewClient(accessToken, gitlab.WithBaseURL(apiEndpoint))
	if err != nil {
		fatal(err)
	}

	//fmt.Println("Running ", command)

	plugin := findPlugin(command)

	plugInstance, err := plugin.Lookup("Instance")
	if err != nil {
		fatal(err)
	}
	*plugInstance.(*string) = instance
	if err != nil {
		fatal(err)
	}
	plugGitlabClient, err := plugin.Lookup("GitlabClient")
	if err != nil {
		fatal(err)
	}
	*plugGitlabClient.(*gitlab.Client) = *gitlabClient
	plugGraphqlClient, err := plugin.Lookup("GraphqlClient")
	if err != nil {
		fatal(err)
	}
	*plugGraphqlClient.(*graphql.Client) = *graphqlClient

	runner, _ := plugin.Lookup("RunCheck")
	runner.(func())()

}
func main() {

	if len(os.Args) > 1 {
		switch command := os.Args[1]; command {

		case "list":
			listPlugins()
		case "help":
			printHelp()
		default:
			loadAndRun(command)
		}
	} else {
		printHelp()
	}
}
