FROM golang:latest
RUN apt-get update
RUN apt-get -y install jq
WORKDIR /go/src/app
COPY . .

RUN make

CMD ["./build/ethan"]
