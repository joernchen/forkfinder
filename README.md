## ETHAN

**E**nhanced **T**ool for **H**ardening and **A**uditing an I**N**stance

This tool aims to provide a plugin based framework for
various security checks to be run against a GitLab instance.

### Plugins

Plugins are to be placed into a subfolder under `./plugins`. A very
short sample plugin would look like this:

```go
package main

import (

	"fmt"
	"github.com/shurcooL/graphql"
	"github.com/xanzy/go-gitlab"
	"os"

)

var Instance string // target instance will be set before invoking RunCheck
var GitlabClient gitlab.Client // GitLab API client will be set before invoking RunCheck
var GraphqlClient graphql.Client // GitLab Graphql API client will be set before invoking RunCheck

var PluginCommand = "sample"  // command to be called via the CLI
var PluginDesc = "This command is a sample" // description of the command

var PluginHelp = fmt.Sprintf("%s %s", os.Args[0], PluginCommand) // usage / help text



func RunCheck() {

	// here the actual check is to be implemented
	// it might use Target, Instance, GitlabClient 
	// and GraphqlClient to implement any checks
	// on the instance
   fmt.Println("Hello!")
}
```

Plugins can take their options from `os.Args[2]` onwards.

### Building

The included `Makefile` will compile `ETHAN` and all plugins.
The resulting binaries will end up in `./build`.

### Usage

The main `ETHAN` binary will take care of setting up the REST and
Graphql clients. To do so it needs the following environment variables
to be set:

* GL_API_HOST - the target instance's base URL
* GL_API_TOKEN - a personal access token for that instance


### Tests

TBD
